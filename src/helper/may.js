import mUserCurrent from 'model/user/current';

export default function() {
    return mUserCurrent.may.apply(mUserCurrent, arguments);
}
