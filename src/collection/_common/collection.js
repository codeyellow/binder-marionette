import { Collection } from 'backbone-crux';

define(function (require) {
    'use strict';

    var Backbone = require('backbone'),
        Model = require('model/_common/model'),
        config = require('config/app'),
        _ = require('underscore'),
        $ = require('jquery');

    return Collection.extend({
        model: Model,
        /**
         * This should be in collection.attributes, but since crux hardcodes
         * the creation of a Backbone.Model, we cannot extend it.
         *
         * This sets attributes[key] to value if not set before. If it does,
         * it will cast attributes[key] to an array and append value to it.
         *
         * @param {String} key
         * @param {mixed} value
         */
        append: function (key, value) {
            var originalVal = this.attributes.get(key);

            // Append to original value if it existed.
            if (originalVal) {
                if (Array.isArray(originalVal)) {
                    originalVal.push(value);
                    value = originalVal;
                } else {
                    value = [originalVal, value];
                }
            }

            // Unset & set to make sure a change is triggered.
            this.attributes.unset(key, { silent: true });
            this.attributes.set(key, value);
            this.trigger('fetch');
        },
        fetchData: function () {
            var data = Collection.prototype.fetchData.call(this);

            return _.defaults(data, this.getLimitOffsetFromState());
        },
        /**
         * Don't use backbone.paginator fetch because we pass data as a string.
         * Backbone.Paginator expects data to always be an object.
         *
         * @param {Object}
         * @return {jqXHR}
         */
        fetch: function (options) {
            var defaults = {
                // Get data for fetch.
                data: this.getFormattedFetchData(),
            };

            this.xhr = Backbone.Collection.prototype.fetch.call(this, _.extend(defaults, options));

            this.xhr.fail(() => {
                vent.trigger('after:collection:read:error');
            })

            return this.xhr;
        },
        /**
         * Translates the data kept on the front-end to a string which the
         * server understands.
         *
         * @param {object} data
         * @return {string}
         */
        formatFetchData: function (data) {
            // Make sure sort order is always consistent by ordering on -id as
            // well if needed.
            if (Array.isArray(data.order_by)) {
                if (data.order_by.indexOf('id') === -1 && data.order_by.indexOf('-id') === -1) {
                    const orderBy = data.order_by.slice();
                    orderBy.push('-id');
                    data.order_by = orderBy.join(',');
                }
            } else {
                data.order_by = '-id';
            }

            _.each(data, function (val, key) {
                if (key.split(':').pop() === 'in' || key === 'with') {
                    if (Array.isArray(val)) {
                        data[key] = val.join(',');
                    }
                }
            });

            return $.param(data, true);
        },
        getFormattedFetchData: function () {
            return this.formatFetchData(this.fetchData());
        },
        /**
         * Converts page and page_size to limit and offset.
         */
        getLimitOffsetFromState: function () {
            var limit = this.attributes.get('limit') === 'none' ? 'none' : null,
                offset = this.state.pageSize * (this.state.currentPage - 1);

            if (limit === null) {
                limit = this.state.pageSize ? this.state.pageSize : config.PAGE_SIZE;
            }

            if (!offset) {
                offset = 0;
            }

            return {
                limit: limit,
                offset: offset
            };
        },
        parseState: function (resp, queryParams, state, options) {
            if (resp.meta) {
                return {
                    totalRecords: resp.meta.total_records
                };
            } else {
                return {};
            }
        },
        parseRecords: function (resp, options) {
            const HashMap = function(data) {
                this.get = function(id) {
                    return this.byId[id];
                };

                this.add = function(someData) {
                    _.each(someData, (item) => {
                        this.byId[item.id] = item;
                    });
                };

                this.byId = {};
                this.add(data);
            };

            if (resp.with) {
                options.repository = new Backbone.Collection();

                _.each(resp.with, (data, key) => {
                    const withMapping = [key];
                    _.each(resp.with_mapping, (givenKey, requestedKey) => {
                        if (givenKey === key) {
                            withMapping.push(requestedKey.split('.').pop());
                        }
                    });

                    _.each(_.uniq(withMapping), (id) => {
                        if (options.repository.get(id)) {
                            options.repository.get(id).get('collection').add(data);
                        } else {
                            options.repository.add({
                                id,
                                collection: new HashMap(data),
                            });
                        }
                    });
                });
            }

            return Collection.prototype.parseRecords.call(this, resp, options);
        },
        /**
         * TODO: Move to crux.
         * @return {Array}
         */
        toHuman: function () {
            var result = [];

            this.each(function (m) {
                result.push(m.toHuman());
            });

            return result;
        },
        /**
         * Upon delete, the view can choose to keep the model in the collection
         * with options.keep = true:
         *
         * this.model.destroy({keep: true});
         */
        remove: function(models, options) {
            options = options || {};

            if (options.keep) {
                return;
            } else {
                return Collection.prototype.remove.call(this, models, options);
            }
        },
        copy() {
            const models = [];

            _.each(this.models, (model) => {
                models.push(model.copy());
            });

            return new this.constructor(models);
        },
        /**
         * Overwrite indexOf because Backbone's implementation is slow...
         */
        indexOf(model) {
            if (model && model.id) {
                return this.pluck('id').indexOf(model.id);
            }

            return Collection.prototype.indexOf.call(this, model);
        },
    });
});
