import Uri from 'urijs';
import isSimilar from 'exports?isSimilar!isSimilar';

// Stolen from https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/btoa.
// ucs-2 string to base64 encoded ascii
function utoa(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

// base64 encoded ascii to ucs-2 string
function atou(str) {
    return decodeURIComponent(escape(window.atob(str)));
}

export default class extends Marionette.Object {
    initialize(collection) {
        this.collection = collection;
        this.attributes = collection.attributes;
        this.state = collection.state;

        this.attributes.listenTo(collection, 'before:read', this.navigate.bind(this));
        collection.listenTo(collection, 'sync', this.onSync.bind(this));
    }
    parse(search, defaults) {
        if (Uri.parseQuery(search).state) {
            const params = JSON.parse(decodeURIComponent(atou(Uri.parseQuery(search).state)));
            defaults = defaults || {};

            // Inherit the default page state settings from Paginator if none are given.
            defaults.page = defaults.page || this.state.currentPage;
            defaults.per_page = defaults.per_page || this.state.pageSize;

            this.attributes.set(_.extend({}, defaults, params));

            this.state.pageSize = this.attributes.get('per_page');
            this.state.currentPage = this.attributes.get('page');

            return true;
        }

        return false;
    }
    getUrl() {
        // TODO: This will overwrite existing search params.
        // It should extend from Backbone.history.location.search
        const state = this.attributes.toJSON();
        const search = JSON.stringify(state);
        const pathname = Backbone.history.location.hash.split('?')[0];

        return pathname + '?state=' + encodeURIComponent(utoa(search));
    }
    setLastState(state) {
        this.lastState = JSON.parse(JSON.stringify(state));
    }
    /**
     * Update the current page information after a fetch.
     */
    onSync(collection) {
        // The models in the collection can trigger a sync event as well. Make
        // sure the event comes from the collection itself.
        if (collection === this.collection) {
            this.attributes.set({
                page: collection.state.currentPage,
                per_page: collection.state.pageSize,
            }, { silent: true });
        }
    }
    navigate() {
        const firstNavigate = !this.lastState;

        this.setLastState(this.attributes.toJSON());

        // On first navigate, replace the url so the back button works properly.
        Backbone.history.navigate(this.getUrl(), firstNavigate ? { replace: true } : {});
    }
    isStateChanged() {
        return !isSimilar(this.attributes.toJSON(), this.lastState);
    }
}
