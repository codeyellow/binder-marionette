import Marionette from 'marionette';
import bootstrapper from 'bootstrapper';

// Create new app.
const app = new Marionette.Application();

app.addRegions({
    header: '#_header',
    content: '#_content',
    footer: '#_footer',
    notification: '#_notification',
    modal: '#_modal',
});

// Add hack to display url in title. Handy for history navigation.
const $title = $('title');
const originalTitle = $title.text();

app.listenTo(Backbone.history, 'all', () => {
    const url = Backbone.history.location.hash.slice(1);
    $title.text(originalTitle + ' ' + url);
});

/**
 * Clean up app to init state.
 */
app.cleanup = function () {
    var VHeader = require('view/header'),
        vNotification = require('view/notification');

    this.header.show(new VHeader({ model: mUserCurrent }));
    this.content.empty();
    this.notification.show(vNotification);
};

app.setupEnvBar = function () {
    var $ = require('jquery');

    // Apply css string. Parse to avoid CSP errors.
    // TODO: make better one day if I don't have a deadline
    // in 10 min.
    bootstrapper.env_bar_css.split(';').forEach(css => {
        const parts = css.split(':');

        if (parts.length === 2) {
            $('#_environment-bar').css(parts[0].trim(), parts[1].trim());
        }
    });

    $('#_environment-bar-movetop').click(function () {
        $('#_environment-bar').css('transform', 'rotate(0) translate(0, 0)')
        $('#_environment-bar-movetop').hide();
        $('#_environment-bar-moveside').show();
    });
    $('#_environment-bar-moveside').click(function () {
        $('#_environment-bar').css('transform', 'rotate(90deg) translate(100%, 0)')
        $('#_environment-bar-moveside').hide();
        $('#_environment-bar-movetop').show();
    });

    app.updateEnvBar();
}

app.updateEnvBar = function () {
    var mUserCurrent = require('model/user/current'),
        $ = require('jquery'),
        bs = bootstrapper,
        content,
        userdesc,
        groups;

    if (mUserCurrent.isLoggedIn()) {
        userdesc = mUserCurrent.get('id') + '/' + mUserCurrent.get('username');
        groups = mUserCurrent.get('is_superuser') ? ['superuser'] : [];
        groups = groups.concat(mUserCurrent.get('group_names'));
        userdesc += ' (' + groups.join(',') + ')';
    } else {
        userdesc = 'anonymous';
    }
    content = bs.env_name;
    content += ' &nbsp; :: &nbsp; ' + bs.branch;
    content += ' &nbsp; :: &nbsp; ' + bs.version;
    content += ' &nbsp; :: &nbsp; ' + (bs.commit_nr ? bs.commit_nr + '-' : '');
    content += (bs.commit_hash ? bs.commit_hash.substring(0,12) : 'undefined');
    content += ' &nbsp; :: &nbsp; ' + (bs.debug ? 'DEBUG' : 'debug off');
    content += ' &nbsp; :: &nbsp; ' + userdesc;
    content += ' &nbsp; :: &nbsp; ' + bs.csrf_token;

    $('#_environment-bar-content').html(content);
}

/**
 * Start the app and route to correct page. This function is called after the
 * user is logged in and all front end data is cached.
 *
 * You can assume that unauthenticated users will never start the actual app.
 */
function launchApplication() {
    // When you get to this point, you are authenticated. Remove special login
    // class.
    $('body').removeClass('login');
    app.updateEnvBar();
    app.cleanup();
    fireInitialRoute();
}

function fireInitialRoute() {
    const router = require('router/index');
    const lastRoute = router.getLastRoute();

    // Redirect after login with requested routed.
    if (lastRoute === 'user/login(/*requestedRoute)') {
        return Backbone.history.navigate(window.location.hash.substr('#user/login/'.length), true);
    }

    // Give control to default controller. It will redirect user to their
    // default home route.
    Backbone.history.navigate('', true);
};

app.addInitializer(() => {
    app.cleanup();
    app.setupEnvBar();

    // Check if user is logged in, and trigger event.
    if (mUserCurrent.isLoggedIn()) {
        launchApplication();
    }
});

app.listenTo(mUserCurrent, 'after:login:success', () => {
    const csrfToken = require('jquery-csrf-token');

    // Always refetch bootstrapper after login to reset csrf token.
    app.fetchBootstrap().done(data => {
        bootstrapper.setData(data);
        csrfToken.setToken(data.csrf_token);
        launchApplication();
    });
});

app.fetchBootstrap = function () {
    return $.get('api/bootstrap/');   
}

app.listenTo(mUserCurrent, 'after:logout:success', app.cleanup);

export default app;
