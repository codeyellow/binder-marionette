import app from 'app';
import { enable as enableToken } from 'jquery-csrf-token';
import bootstrapper from 'bootstrapper';

export default function(data) {
    if (data.user) {
        // The { parse: true } must be present so that filter_options will be
        // parsed.
        mUserCurrent.set(mUserCurrent.parse(data.user), { parse: true });
    }

    bootstrapper.setData(data);

    enableToken(data.csrf_token, {
        key: 'X-CSRFTOKEN',
        retry: {
            url: 'api/bootstrap/',
            parseResponse: resp => resp.csrf_token,
            isCSRFFailure: resp => resp.status === 403 && resp.responseJSON.code === 'CSRFFailure',
        },
    });

    app.start(bootstrapper);
    Backbone.history.start();
}
