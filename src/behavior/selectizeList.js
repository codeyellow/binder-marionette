import selectize from 'backbone-selectize';

/**
 * Works, but needs some improvements.
 *
 * options:
 *  - cSelectize (as view.options or attribute)
 *  - inputSelector as option
 *
 */
export default Marionette.Behavior.extend({
    defaults() {
        return {
            selectizeOptions: {
                value: 'id',
                text: 'name',
                placeholder: 'Begin typing...',
                searchParameter: 'search',
            },
        };
    },
    plugins: {
        selectize: {
            bind() {
                selectize(this.getSelectizeOptions());
                this.updateSelectedItems();
            },
            unbind() {

            },
        },
    },
    ui: {
        selectizeList: '[name=client]',
    },
    collectionEvents: {
        'remove': 'removeFromSelectize',
    },
    initialize() {
        this.listenTo(this.getSelectizeCollection(), 'update', this.updateSelectedItems);
    },
    getSelectizeOptions() {
        const selectizeOptions = {
            ui: this.get$input(),
            collection: this.getSelectizeCollection(),
            search: true,
            onDelete: () => false,
            searchOptions: { remove: false },
            onItemAdd: (value) => {
                this.view.collection.add(this.getSelectizeCollection().get(value).clone());
            },
        };

        _.extend(selectizeOptions, this.options.selectizeOptions);

        return selectizeOptions;
    },
    get$input() {
        return this.view.$(this.options.inputSelector);
    },
    getSelectizeCollection() {
        return this.view.getOption('cSelectize');
    },
    removeFromSelectize(mSelectedItem) {
        this.get$input()[0].selectize.removeItem(mSelectedItem.id);
    },
    updateSelectedItems() {
        const selectizeOptions = this.getSelectizeOptions();
        this.getSelectizeCollection().add(this.view.collection.toJSON());

        // Add already added contacts to selectize. This way selectize
        // will correctly filter out contacts that we've already added.
        this.view.collection.each((mProduct) => {
            const data = this.get$input()[0].selectize.settings.formatOption(mProduct, selectizeOptions);

            this.get$input()[0].selectize.addOption(data);
            this.get$input()[0].selectize.addItem(mProduct.id);
        });
    },
});
