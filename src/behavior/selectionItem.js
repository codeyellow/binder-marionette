export default Marionette.Behavior.extend({
    ui: {
        selectCheckbox: '[name="selectItem"]',
    },
    events: {
        'change @ui.selectCheckbox': 'toggleSelection',
    },
    modelEvents: {
        'selectItem:add': 'markChecked',
        'selectItem:remove': 'markUnchecked',
    },
    defaults: {
        isSelectable() {
            return true;
        },
    },
    initialize(options) {
        this.listenTo(this.view.options.model.collection, 'selectItem:check:on', this.checkOn);
        this.listenTo(this.view.options.model.collection, 'selectItem:check:off', this.checkOff);
        this.listenTo(this.view.options.model.collection, 'selectItem:check:render', this.checkRender);
    },
    onRender() {
        if (!this.options.isSelectable.call(this.view)) {
            this.ui.selectCheckbox.prop('disabled', true);
        }
    },
    toggleSelection() {
        if (this.ui.selectCheckbox.is(':checked')) {
            this.checkOn();
        } else {
            this.checkOff();
        }
    },
    checkOn() {
        if (!this.ui.selectCheckbox.prop('disabled')) {
            this.ui.selectCheckbox.prop('checked', true);
            this.view.model.trigger('selectItem:add', this.view.model);
        }
    },
    checkOff() {
        if (!this.ui.selectCheckbox.prop('disabled')) {
            this.ui.selectCheckbox.prop('checked', false);
            this.view.model.trigger('selectItem:remove', this.view.model);
        }
    },
    checkRender(selectedIds) {
        if (selectedIds.indexOf(this.view.model.id) !== -1) {
            this.ui.selectCheckbox.prop('checked', true);
            this.markChecked();
        } else {
            this.ui.selectCheckbox.prop('checked', false);
            this.markUnchecked();
        }
    },
    markChecked() {
        this.view.$el.addClass('selected');
    },
    markUnchecked() {
        this.view.$el.removeClass('selected');
    },
});
