export default Marionette.Behavior.extend({
    ui: {
        masqueradeButton: '._masquerade',
    },
    events: {
        'click @ui.masqueradeButton': 'masquerade',
    },
    masquerade() {
        const xhr = $.ajax({
            url: `api/user/${this.view.model.id}/masquerade/`,
            method: 'POST',
            contentType: 'application/json',
        });

        if (xhr) {
            xhr.fail(vent.notify('user:masquerade:error'));
            xhr.done(() => {
                location.reload();
            });
        }

        return xhr;
    },
});
