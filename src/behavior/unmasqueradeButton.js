export default Marionette.Behavior.extend({
    ui: {
        unmasqueradeButton: '._unmasquerade',
    },
    events: {
        'click @ui.unmasqueradeButton': 'unmasquerade',
    },
    unmasquerade() {
        const xhr = $.ajax({
            url: 'api/user/endmasquerade/',
            method: 'POST',
            contentType: 'application/json',
        });

        if (xhr) {
            xhr.fail(vent.notify('user:unmasquerade:error'));
            xhr.done(() => {
                location.reload();
            });
        }

        return xhr;
    },
});
