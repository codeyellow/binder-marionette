import isSimilar from 'exports-loader?isSimilar!isSimilar';

export default Marionette.Behavior.extend({
    defaults: {
        apply() {
            // Nop by default.
        },
        renderCount($el) {
            // Nop by default.
        },
        isChanged(view) {
            return isSimilar(view.options.state.lastState, view.collection.attributes.toJSON());
        },
    },
    ui: {
        applyButton: '._apply',
        filterApplyMatchCount: '._filter-apply-match-count',
    },
    events: {
        'click @ui.applyButton': 'apply',
    },
    initialize() {
        this.listenTo(this.view.options.collection.attributes, 'change', this.maybeEnableApplyButton);
        this.listenTo(this.view.options.collection, 'after:read', this.maybeEnableApplyButton);
    },
    onRender() {
        this.maybeEnableApplyButton();
    },
    apply() {
        this.options.apply.call(this.view);
        this.view.triggerMethod('applyButton:applied');
    },
    maybeEnableApplyButton() {
        if (this.view.options.state) {
            if (this.options.isChanged(this.view)) {
                this.ui.applyButton.prop('disabled', true);
            } else {
                this.ui.applyButton.prop('disabled', false);
            }

            // Hmm, also do this when the apply button is disabled? Well, what else should we do?
            this.options.renderCount.call(this.view, this.ui.filterApplyMatchCount);
        }
    },
});
