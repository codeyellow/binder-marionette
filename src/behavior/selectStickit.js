/**
 * Creates a stickit + multiselect filter combo.
 *
 * Extend and define:
 *
 * required to fill in
 * - createSourceCollection
 * - filterKey
 * - ui.selectStickit
 *
 * optional:
 * - multiselectOptions
 * - multiselectFilterOptions
 */
export default Marionette.Behavior.extend({
    filterKey: '',
    cSource: null,
    initialize() {
        this.cSource = this.createSourceCollection(this.view);
        this.setDefaultAttributes(this.cSource);

        this.listenTo(this.cSource, 'refetch', this.refetch);
    },
    createSourceCollection(view) {

    },
    getTargetModel(view) {
        return view.collection.attributes;
    },
    fetch() {
        // Set silent: true, so that backbone.stickit won't rerender the select
        // on each add. Afterwards we set trigger a fake reset so stickit will
        // pick up the change.
        return this.cSource.fetch({ silent: true });
    },
    afterFetch() {
        this.setInitialState();
        this.cSource.trigger('reset');
    },
    refetch() {
        const xhr = this.fetch();

        if (xhr && xhr.done) {
            xhr.done(() => this.afterFetch());
        } else {
            this.afterFetch();
        }
    },
    setDefaultAttributes(cSource) {
        cSource.attributes.set('limit', 'none');
        cSource.attributes.set('order_by', 'name');
    },
    setInitialState() {
        const mTarget = this.getTargetModel(this.view);

        if (mTarget.get(this.filterKey) === undefined) {
            mTarget.set(this.filterKey, this.cSource.pluck('id'));
        }
    },
    onRender() {
        this.refetch();
    },
    multiselectOptions: {
        selectedText: '',
        noneSelectedText: '',
    },
    multiselectFilterOptions: {
        label: '',
        placeholder: '',
        width: 218,
    },
    stickitSelectOptions: {
        valuePath: 'id',
        labelPath: 'name',
    },
    bindingOptions() {
        const selectOptions = _.extend({
            collection: this.cSource,
        }, this.getOption('stickitSelectOptions'));

        return {
            observe: this.filterKey,
            selectOptions,
            onGet(val) {
                if (val instanceof Backbone.Collection) {
                    return val.pluck('id');
                }

                return val;
            },
            initialize: $el => {
                const multiselectOptions = this.getOption('multiselectOptions');
                const multiselectFilterOptions = this.getOption('multiselectFilterOptions');

                $el.multiselect(multiselectOptions).multiselectfilter(multiselectFilterOptions);
            },
            destroy: $el => {
                $el.multiselect('destroy');
            },
            update: ($el, val, model, options) => {
                Backbone.Stickit._handlers[5].update($el, val, model, options);

                // Make sure multiselect exists.
                if ($el.multiselect('instance')) {
                    $el.multiselect('refresh');
                }
            },
        };
    },
    plugins() {
        return {
            stickit: {
                bind() {
                    this.view.addBinding(
                        this.getTargetModel(this.view),
                        this._uiBindings.selectStickit,
                        this.bindingOptions()
                    );
                },
                unbind() {
                    this.view.unstickit(this.getTargetModel(this.view), this._uiBindings.selectStickit);
                },
            },
        };
    },
});
