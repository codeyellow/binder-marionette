import Collection from 'collection/_common/collection';

export default Marionette.Behavior.extend({
    ui: {
        checkAll: '[name="checkall"]',
        count: '._select-item-count',
        plurar: '._select-item-plurar',
    },
    events: {
        'change @ui.checkAll': 'toggleCheckAll',
    },
    initialize() {
        this.cSelection = this.getOption('getSelectionCollection')(this.view);
        this.view.cSelection = this.cSelection;

        this.listenTo(this.cSelection, 'add remove reset', this.renderCount);
        this.listenTo(this.cSelection, 'add remove reset', this.maybeShowPlurar);

        // Add reference to cSelection. Don't hate me for this.
        this.view.cSelection = this.cSelection;

        this.cTrack = this.getOption('getTrackCollection')(this.view);

        this.listenTo(this.cTrack, 'selectItem:add', this.addToSelection);
        this.listenTo(this.cTrack, 'selectItem:remove', this.removeFromSelection);
        this.listenTo(this.cTrack, 'after:read', this.clearSelection);
    },
    getSelectionCollection(view) {
        return new Collection();
    },
    getTrackCollection(view) {
        return view.collection || view.options.collection;
    },
    addToSelection(model) {
        this.cSelection.add(model);
    },
    removeFromSelection(model) {
        this.cSelection.remove(model);
    },
    clearSelection() {
        this.cSelection.reset();
    },
    toggleCheckAll() {
        if (this.ui.checkAll.is(':checked')) {
            this.cTrack.trigger('selectItem:check:on');
        } else {
            this.cTrack.trigger('selectItem:check:off');
        }
    },
    renderCount() {
        this.ui.count.text(this.cSelection.length);
    },
    maybeShowPlurar() {
        this.ui.plurar.toggle(this.cSelection.length > 1);
    },
    onSelectionTrackerRender() {
        this.cTrack.trigger('selectItem:check:render', this.cSelection.pluck('id'));
    },
});
