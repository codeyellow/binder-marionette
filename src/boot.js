import 'init';
import app from 'app';
import start from './start';
import tXhrError from 'view/xhr-error.html';

const xhr = app.fetchBootstrap();

function error() {
    const errorTemplate = tXhrError({
        status: xhr.status,
    });

    // A real error occurred;
    $('body').addClass('big-error').html(errorTemplate);
}

xhr.done(start);
xhr.fail(error);
