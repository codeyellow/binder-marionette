import tChangePassword from './changePassword.html';
import ThrottleBehavior from 'marionette-throttle';

export default Marionette.ItemView.extend({
    behaviors: {
        throttle: {
            methods: ['changePassword'],
            behaviorClass: ThrottleBehavior,
        },
    },
    template: tChangePassword,
    ui: {
        oldPasswordInput: '[name="old_password"]',
        newPasswordInput: '[name="new_password"]',
        errorMessage: '._error-message',
        successMessage: '._success-message',
        saveButton: '._save',
    },
    events: {
        'submit form': 'changePassword',
        'click @ui.saveButton': 'changePassword',
    },
    onShow() {
        this.ui.oldPasswordInput.focus();
        this.ui.errorMessage.hide();
    },
    changePassword(event) {
        event.preventDefault();
        this.ui.errorMessage.hide();

        const xhr = $.ajax({
            url: this.model.urlRoot + 'change_password/',
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({
                old_password: this.ui.oldPasswordInput.val(),
                new_password: this.ui.newPasswordInput.val(),
            }),
        });

        if (xhr) {
            xhr.fail(vent.notify('user:changePassword:error'));
            xhr.done(() => {
                vent.trigger('user:changePassword:success');
                this.ui.oldPasswordInput.val('');
                this.ui.newPasswordInput.val('');
            });
        }

        return xhr;
    },
});
