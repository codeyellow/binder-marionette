import ThrottleBehavior from 'marionette-throttle';
import tEdit from 'view/user/edit.html';

export default Marionette.ItemView.extend({
    behaviors: {
        throttle: {
            methods: ['save'],
            behaviorClass: ThrottleBehavior,
        },
    },
    template: tEdit,
    plugins() {
        return {
            multiselect: {
                bind() {
                    this.ui.groupSelect.multiselect({
                        selectedText: t('user.field.groups.multiselect.selectedText'),
                        noneSelectedText: t('user.field.groups.multiselect.noneSelectedText'),
                    }).multiselectfilter({
                        label: '',
                        placeholder: t('user.field.groups.multiselect.placeholder'),
                        width: 218,
                    });
                },
                unbind() {
                    this.ui.groupSelect.multiselect('destroy');
                },
            },
            stickit: {
                bind() {
                    this.stickit();
                },
                unbind() {
                    // Nop.
                },
            },
        };
    },
    ui: {
        'usernameInput': '[name="username"]',
        'passwordInput': '[name="password"]',
        'firstNameInput': '[name="first_name"]',
        'lastNameInput': '[name="last_name"]',
        'groupSelect': '[name="groups"]',
        'saveButton': '._save',
    },
    events: {
        'click @ui.saveButton': 'save',
    },
    bindings() {
        return {
            '@ui.usernameInput': 'username',
            '@ui.passwordInput': 'password',
            '@ui.firstNameInput': 'first_name',
            '@ui.lastNameInput': 'last_name',
            '@ui.groupSelect': {
                observe: 'groups',
                selectOptions: {
                    collection: this.options.cGroup,
                    labelPath: 'name',
                    valuePath: 'id',
                },
                onGet(val) {
                    return val.pluck('id');
                },
                /**
                 * {multiselect-hack-321}
                 * Hack to update multiselect if an option is added.
                 */
                update: ($el, val, model, options) => {
                    Backbone.Stickit._handlers[5].update($el, val, model, options);
                    this.debounceMultiselectRefresh();
                },
            },
        };
    },
    debounceMultiselectRefresh: _.debounce(function() {
        this.ui.groupSelect.multiselect('refresh');
    }, 100),
    save() {
        return this.model.save()
            .fail(vent.notify('user:save:error'))
            .done(() => {
                vent.trigger('user:save:success');
                Backbone.history.navigate('#data/user/' + this.model.id + '/edit', { replace: true });
            });
    },
});
