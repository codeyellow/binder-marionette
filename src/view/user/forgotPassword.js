import tForgotPassword from './forgotPassword.html';
import ThrottleBehavior from 'marionette-throttle';

export default Marionette.ItemView.extend({
    behaviors: {
        throttle: {
            methods: ['sendResetLink'],
            behaviorClass: ThrottleBehavior,
        },
    },
    template: tForgotPassword,
    ui: {
        usernameInput: '[name="username"]',
        errorMessage: '._error-message',
        successMessage: '._success-message',
    },
    events: {
        'submit form': 'sendResetLink'
    },
    onShow: function () {
        this.ui.usernameInput.focus();
        this.ui.errorMessage.addClass('hide');
    },
    sendResetLink: function (event) {
        event.preventDefault();
        this.ui.errorMessage.addClass('hide');
        this.ui.successMessage.addClass('hide');

        var xhr = $.ajax({
            method: 'POST',
            url: this.model.urlRoot + 'reset_request/',
            data: JSON.stringify({
                username: this.ui.usernameInput.val(),
            }),
            contentType: 'application/json',
            processData: false,
        });

        if (xhr) {
            xhr.fail(() => { this.ui.errorMessage.removeClass('hide');; });
            xhr.done(() => { this.ui.successMessage.removeClass('hide'); });
        }

        return xhr;
    }
});
