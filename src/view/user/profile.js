import UnmasqueradeButton from 'binder-marionette/src/behavior/unmasqueradeButton';
import tProfile from './profile.html';

export default Marionette.ItemView.extend({
    behaviors: {
        unmasqueradeButton: { behaviorClass: UnmasqueradeButton },
    },
    template: tProfile,
});
