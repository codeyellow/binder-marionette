import mPhone from '../model/phone';
import tPhone from './phone.html';

export default Marionette.ItemView.extend({
    durationTimer: null,
    iconHoldClass: 'icon-hold',
    iconUnholdClass: 'icon-unhold',
    onHold: false,
    model: mPhone,
    getNumber() {},
    getExtraHeaders() {},
    template: tPhone,
    templateHelpers() {
        return {
            defaultHoldIcon: this.getOption('iconHoldClass'),
        };
    },
    ui: {
        callButton: '._call',
        hangupButton: '._hangup',
        duration: '._duration',
        holdButton: '._hold',
    },
    events: {
        'click @ui.callButton': 'call',
        'click @ui.hangupButton': 'hangup',
        'click @ui.holdButton': 'hold',
    },
    modelEvents: {
        'change:status': 'renderPhone',
    },
    bindings: {
        ':el': {
            observe: 'status',
            update($el, val) { $el.alterClass('status-*', 'status-' + s.slugify(val)); },
        },
    },
    plugins() {
        return {
            stickit: {
                bind() {
                    this.stickit();
                },
                unbind() {
                    // NOP.
                },
            },
        };
    },
    initialize() {
        // This will set up the connection if it wasn't set up before.
        this.model.getPhone();
    },
    onDestroy() {
        window.clearInterval(this.durationTimer);
    },
    onRender() {
        this.renderPhone();

        if (!this.getOption('enableHold')) {
            this.ui.holdButton.hide();
        }
    },
    renderPhone() {
        switch (this.model.get('status')) {
            case this.model.constructor.STATUS_READY:
                if (this.durationTimer) {
                    window.clearInterval(this.durationTimer);
                    this.durationTimer = null;
                }

                this.ui.callButton.show();
                this.ui.hangupButton.hide();
                this.ui.holdButton.hide();
                break;
            case this.model.constructor.STATUS_DIALING:
                this.startOfCall = moment();
                this.durationTimer = window.setInterval(() => this.renderDuration(), 1000);
                this.renderDuration();
                this.ui.callButton.hide();
                this.ui.hangupButton.show();
                this.ui.holdButton.hide();
                break;
            case this.model.constructor.STATUS_IN_CALL:
                if (this.onHold) {
                    this.onHold = false;
                } else {
                    if (this.durationTimer) {
                        window.clearInterval(this.durationTimer);
                        this.durationTimer = null;
                    }
                    this.startOfCall = moment();
                    this.durationTimer = window.setInterval(() => this.renderDuration(), 1000);
                    this.renderDuration();
                }
                this.ui.callButton.hide();
                this.ui.hangupButton.show();
                if (this.getOption('enableHold')) {
                    this.ui.holdButton.show();
                }
                break;
            case this.model.constructor.STATUS_ON_HOLD:
                this.onHold = true;
                this.ui.callButton.hide();
                this.ui.hangupButton.show();
                if (this.getOption('enableHold')) {
                    this.ui.holdButton.show();
                }
                break;
            default:
                console.log('Unkown phone status ' + this.model.get('status'));
                break;
        }
    },
    renderDuration() {
        const duration = moment.duration(moment().diff(this.startOfCall, 'seconds'), 'seconds');
        this.ui.duration.text(duration.format('m:ss', { trim: false }));
    },
    hangup() {
        this.model.getPhone().phoneCalls.forEach(phoneCall => phoneCall.terminate());
        if (this.getOption('enableHold')) {
            this.resetHold();
        }
    },
    call() {
        this.model.getPhone().startCall(this.getOption('getNumber')(), {
            extraHeaders: this.getOption('getExtraHeaders')(),
        });
        this.model.setVoiceVolume(1);
    },
    hold() {
        const holdIcon = this.getOption('iconHoldClass');
        const unholdIcon = this.getOption('iconUnholdClass');
        switch (this.model.get('status')) {
            case this.model.constructor.STATUS_IN_CALL:
                this.model.getPhone().phoneCalls.forEach(phoneCall => phoneCall.hold());
                this.ui.holdButton.removeClass(holdIcon).addClass(unholdIcon);
                break;
            case this.model.constructor.STATUS_ON_HOLD:
                this.model.getPhone().phoneCalls.forEach(phoneCall => phoneCall.unhold());
                this.ui.holdButton.removeClass(unholdIcon).addClass(holdIcon);
                break;
            default:
                break;
        }
    },
    resetHold() {
        this.ui.holdButton.removeClass(this.getOption('iconUnholdClass')).addClass(this.getOption('iconHoldClass'));
        this.onHold = false;
    },
});
