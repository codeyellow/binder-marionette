import VAvailable from './myView/available';
import VSave from './myView/save';
import tMyView from './myView.html';

export default Marionette.LayoutView.extend({
    template: tMyView,
    regions: {
        available: '._available-region',
        save: '._save-region',
    },
    ui: {
        saveButton: '._save',
        deleteButton: '._delete',
    },
    events: {
        'click @ui.saveButton': 'showSave',
    },
    onRender() {
        this.available.show(new VAvailable({
            keys: this.getOption('keys'),
            prefix: this.getOption('prefix'),
            cTarget: this.options.cTarget,
            model: mUserCurrent,
            collection: mUserCurrent.get('filter_options'),
            onChangeView: this.getOption('onChangeView'),
        }));
    },
    onChangeView(cTarget) {
        cTarget.fetch();
    },
    showSave() {
        this.save.show(new VSave({
            keys: this.getOption('keys'),
            prefix: this.getOption('prefix'),
            cProject: this.options.cTarget,
            model: mUserCurrent,
        }));
    },
    setView(view) {
        this.available.currentView.changeView(this.available.currentView.addPrefix(view));
    },
    hasView(view) {
        return !!mUserCurrent.get('filter_options').get(this.available.currentView.addPrefix(view));
    },
    setDefaultView() {
        if (this.hasView(config.MY_VIEW_DEFAULT_VIEW)) {
            this.available.currentView.changeView(this.available.currentView.addPrefix(config.MY_VIEW_DEFAULT_VIEW));

            return true;
        }

        return false;
    },
});
