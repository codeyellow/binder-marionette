import TMyView from './available.html';
import VItem from './item';
import VSave from './save';
import isSimilar from 'exports-loader?isSimilar!isSimilar';
import config from 'config/app';

export default Marionette.CompositeView.extend({
    /** @type {String} The prefix used for the dropdown. An example is "_export-".*/
    prefix: '',
    template: TMyView,
    childView: VItem,
    childViewContainer: '._child-view-container',
    childViewOptions() {
        return {
            prefix: this.getOption('prefix'),
        };
    },
    ui: {
        nameInput: '[name="name"]',
        viewSelect: '._view',
        saveButton: '._save',
        deleteButton: '._delete',
    },
    events: {
        'change @ui.viewSelect': 'changeViewByEvent',
        'click @ui.deleteButton': 'removeFilter',
    },
    collectionEvents: {
        'change': 'maybeSetSelected',
    },
    initialize() {
        this.listenTo(vent, 'myView:set', this.changeView);
        this.listenTo(this.options.cTarget.attributes, 'change', this.maybeSetSelected);
    },
    onRender() {
        this.maybeSetSelected();
    },
    removeFilter() {
        const id = this.ui.viewSelect.val();

        vent.trigger('after:myView:remove', id, this.model.get('filter_options'), this.model.get('filter_options').get(id));
        this.model.get('filter_options').remove(id);
        this.model.saveFilterOptions();
    },
    onAddChild() {
        this.maybeSetSelected();
    },
    onRemoveChild() {
        this.maybeSetSelected();
    },
    /**
     * Returns the keys which define the my view.
     *
     * @return {Array}
     */
    getKeys() {
        return this.getOption('keys') || config.MY_VIEW_KEYS;
    },
    maybeSetSelected() {
        const keys = this.getKeys();
        const attrs = _.pick(this.options.cTarget.attributes.toJSON(), keys);
        let found = false;

        this.model.get('filter_options').each((mFilterOption) => {
            if (!found && this.filter(mFilterOption) && this.match(_.pick(mFilterOption.get('options'), keys), attrs)) {
                this.ui.viewSelect.val(mFilterOption.get('id'));
                found = true;
            }
        });

        if (!found) {
            this.ui.viewSelect.val('custom');
            this.ui.saveButton.css('display', 'inline-block');
            this.ui.deleteButton.css('display', 'none');
        } else {
            this.ui.saveButton.hide();

            // Only show delete button if is a user defined filter.
            if (this.ui.viewSelect.val() !== this.addPrefix(config.MY_VIEW_DEFAULT_VIEW)) {
                this.ui.deleteButton.css('display', 'inline-block');
            } else {
                this.ui.deleteButton.css('display', 'none');
            }
        }

        this.trigger('setSelected');
    },
    /**
     * Matches saved filter options against current filter options.
     *
     * @return {Boolean} True if it's a match, false otherwise.
     */
    match(savedFilterOptions, currentFilterOptions) {
        return isSimilar(savedFilterOptions, currentFilterOptions);
    },
    // {fsdfsdfsdfafas312321312}
    addPrefix(name) {
        return this.getOption('prefix') + name;
    },
    removePrefix: VSave.prototype.removePrefix,
    /**
     * Change to a previously stored filter settings. This will clear out
     * the attributes of the collection and replace them with the stored
     * ones. The attributes are cleared because the drill down filters
     * should also be removed.
     *
     * Config settings:
     *
     * - config.MY_VIEW_IMMUTABLE_KEYS: The keys of the attributes that won't
     *   be cleared.
     * - config.MY_VIEW_KEYS: The keys that are saved in the filter settings
     *   and thus set on the collection.
     *
     */
    changeView(selected) {
        selected = selected === undefined ? this.ui.viewSelect.val() : selected;

        const cTarget = this.options.cTarget;

        // Clear attributes, except with.
        _.each(cTarget.attributes.keys(), (key) => {
            if (config.MY_VIEW_IMMUTABLE_KEYS.indexOf(key) === -1) {
                cTarget.attributes.unset(key);
            }
        });

        const params = _.pick(this.model.get('filter_options').get(selected).get('options'), this.getKeys());

        cTarget.attributes.set(JSON.parse(JSON.stringify(params)));
        this.getOption('onChangeView')(cTarget);
    },
    onChangeView(cTarget) {
        cTarget.fetch();
    },
    changeViewByEvent() {
        this.changeView(this.ui.viewSelect.val());
    },
    filter(model) {
        return this.removePrefix(model.get('id')) !== model.get('id');
    },
});
