define(function (require) {
    'use strict';

    var Marionette = require('marionette'),
        TItem = require('./item.html'),
        VSave = require('./save');

    return Marionette.ItemView.extend({
        prefix: '',
        tagName: 'option',
        template: TItem,
        templateHelpers: function () {
            return {
                id: this.removePrefix(this.model.get('id'))
            };
        },
        onRender: function () {
            this.$el.attr('value', this.model.get('id'));
        },
        removePrefix: VSave.prototype.removePrefix,
    });
});
