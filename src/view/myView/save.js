import MUser from 'model/user'
import TSave from './save.html'
import _s from 'underscore.string'

export default Marionette.ItemView.extend({
    title: 'Save this view as',
    showDefaultViewFirst: true,
    prefix: '',
    template: TSave,
    templateHelpers() {
        return {
            title: this.getOption('title'),
            prefix: this.getOption('prefix'),
            showDefaultViewFirst: this.getOption('showDefaultViewFirst'),
            removePrefix: this.removePrefix.bind(this),
        };
    },
    removePrefix(name) {
        if (!name) {
            name = '';
        }

        const prefix = this.getOption('prefix');

        if (name.indexOf(prefix) === 0) {
            return name.slice(prefix.length);
        }

        return name;
    },
    ui: {
        existingNameInput: '[name="existing_name"]',
        newNameInput: '[name="new_name"]',
        errorMessage: '._error',
        cancelButton: '._cancel'
    },
    events: {
        'submit': 'submit',
        'change @ui.existingNameInput': 'maybeShowNewName',
        'click @ui.cancelButton': 'destroy'
    },
    onRender() {
        this.maybeShowNewName();
    },
    maybeShowNewName(e) {
        if (this.ui.existingNameInput.val() === '') {
            this.ui.newNameInput.show();
        } else {
            this.ui.newNameInput.hide();
        }
    },
    // {fsdfsdfsdfafas312321312}
    addPrefix(name) {
        return this.getOption('prefix') + name;
    },
    submit(e) {
        e.preventDefault();

        var mUserCurrent = this.model,
            disallowed = ['', this.addPrefix('')],
            name = this.ui.existingNameInput.val() || this.ui.newNameInput.val();

        this.ui.errorMessage.hide();

        if (disallowed.indexOf(name.toLowerCase()) === -1) {
            // {fdffgdfgpoijoij13289}
            mUserCurrent.get('filter_options').add({
                id: this.addPrefix(this.removePrefix(name)),
                // {joirgjiog908242}
                options: $.extend(true, {}, this.options.cProject.attributes.toJSON())
            }, {merge: true});

            mUserCurrent.saveFilterOptions()
                .done(function () {
                    this.destroy();
                }.bind(this))
                .fail(function () {
                    this.ui.errorMessage.show();
                }.bind(this)
            );
        } else {
            this.ui.errorMessage.show();
        }
    },
});
