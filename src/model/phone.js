import Model from 'model/_common/model';
import Phone from 'simple-phone-api/dist/phone';

const MPhone = Model.extend({
    /**
     * Keep track if phone is ready for use, eg initialized.
     *
     * @type {Boolean}
     */
    hasBeenInitialized: false,
    phone: null,
    phoneSounds: null,
    /**
     * Keep track if already in initializing to prevent
     * from initializing to often.
     *
     * @type {Boolean}
     */
    isInitializing: false,
    defaults: {
        status: 'ready',
    },
    initialize() {
        this.listenTo(vent, 'after:user:logout:success', this.clearPhone);
    },
    initializePhone(phoneConfig) {
        if (!phoneConfig) {
            phoneConfig = config.PHONE;
        }

        if (!this.isInitializing) {
            this.isInitializing = true;

            this.phone = new Phone.Phone(phoneConfig);

            this.listenTo(this.phone, 'created', data => {
                this.setStatusDialing();

                this.listenTo(data.phoneCall, 'answered', () => this.setStatusInCall());
                this.listenTo(data.phoneCall, 'ended', () => this.setStatusReady());
                this.listenTo(data.phoneCall, 'holdchanged', () => this.changeStatusHold());
            });

            this.phoneSounds = new Phone.PhoneSounds();
            this.phoneSounds.addToPhone(this.phone);
            this.phoneSounds.setVolume(0.2);

            this.hasBeenInitialized = true;
            this.trigger('after:init');

            this.isInitializing = false;
        }

        this.trigger('after:initializePhone');
    },
    getPhone() {
        if (!this.hasBeenInitialized) {
            this.initializePhone();
        }

        return this.phone;
    },
    getPhonesounds() {
        return this.phoneSounds;
    },
    clearPhone() {
        if (this.getPhone()) {
            this.getPhone().destroy();
        }

        this.hasBeenInitialized = false;
    },
    setToneVolume(volume) {
        this.phoneSounds.setVolume(volume);
    },
    /**
     * Currently sets ALL phonecalls volume.
     *
     * @param float volume 0 for off, 1 for full volume.
     */
    setVoiceVolume(volume) {
        _.each(this.getPhone().phoneCalls, function(phonecall) {
            phonecall.setVoiceVolume(volume);
        });
    },
    isStatusReady() {
        return this.get('status') === this.constructor.STATUS_READY;
    },
    setStatusReady() {
        this.set('status', this.constructor.STATUS_READY);
    },
    isStatusDialing() {
        return this.get('status') === this.constructor.STATUS_DIALING;
    },
    setStatusDialing() {
        this.set('status', this.constructor.STATUS_DIALING);
    },
    isStatusInCall() {
        return this.get('status') === this.constructor.STATUS_IN_CALL;
    },
    setStatusInCall() {
        this.set('status', this.constructor.STATUS_IN_CALL);
    },
    changeStatusHold() {
        if (this.get('status') === this.constructor.STATUS_IN_CALL) {
            this.set('status', this.constructor.STATUS_ON_HOLD);
        } else if (this.get('status') === this.constructor.STATUS_ON_HOLD) {
            this.set('status', this.constructor.STATUS_IN_CALL);
        }
    },
}, {
    STATUS_READY: 'ready',
    STATUS_DIALING: 'dialing',
    STATUS_IN_CALL: 'in call',
    STATUS_ON_HOLD: 'on hold',
});

export default new MPhone();
