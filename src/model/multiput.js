import { Model } from 'backbone-crux';

/**
 * Add multiput support. A multiput will send a PUT request to the list endpoint
 * which contains multiple related objects.
 *
 * If objects have negative ids, the server will create them. A idmap will be
 * provided by the backend so we can map the negative ids to positive ids given
 * by the server.
 */
export default Model.extend({
    /**
     * Define this models multiput key which it should look it's id in.
     */
    multiputIdServerKey: undefined,

    /**
     * Add multiput support using options.multiput = [
     *      key,
     *      of,
     *      relation,
     *      or,
     *      {relation: 'object'}
     * ];
     *
     * This only works if options.data is not set, otherwise it will follow
     * the normal save.
     *
     */
    save(attrs, options) {
        options || (options = {});

        if (options.multiput && !options.data) {
            return this.multiput(options.multiput, options);
        }

        return Model.prototype.save.call(this, attrs, options);
    },
    /**
     * Override to make negative ids also count as new.
     *
     * @return {Boolean}
     */
    isNew() {
        const isNew = Model.prototype.isNew.call(this);

        if (!isNew && typeof this.id === 'number' && this.id < 0) {
            return true;
        }

        return isNew;
    },
    /**
     * Maps client negative ids with server positive ids.
     *
     * @param  {Array} relationsUnformatted
     * @param  {Objes} map {foo: [[-1, 1], [-2, 2]], bar: [[-3, 3], [-4, 4]]}
     */
    idMap(relationsUnformatted, map) {
        let mapFlat = [];
        const relations = this.formatRelations(relationsUnformatted);

        // Translate old ids to new ids.
        _.each(relations, (def) => {
            const clientKey = def.relation;
            const serverKey = this.getServerResourceName(clientKey);

            if (map[serverKey]) {
                _.each(map[serverKey], (idMap) => {
                    if (this.get(def.relation) instanceof Backbone.Collection) {
                        const mTask = this.get(clientKey).get(idMap[0]);

                        if (mTask) {
                            if (_.isFunction(def.parseId)) {
                                def.parseId(mTask, idMap[1], map[serverKey]);
                            } else {
                                mTask.set({
                                    id: idMap[1],
                                });
                            }
                        }
                    } else {
                        // This is a temp hack. I'm assuming that only 1 model
                        // is saved per multiput.
                        this.get(def.relation).set(this.get(def.relation).idAttribute, idMap[1]);
                    }
                });
            }

            // You can nest multiputs, so recurse on setting the id.
            if (def.relations) {
                if (typeof this.get(def.relation).idMap === 'function') {
                    this.get(def.relation).idMap(def.relations, map);
                }
            }
        });

        if (_.isObject(map)) {
            _.each(map, m => {
                mapFlat = mapFlat.concat(m);
            });
        } else {
            mapFlat = map;
        }

        _.some(mapFlat, idMap => {
            if (idMap[0] === this.id) {
                this.set(this.idAttribute, idMap[1]);
                return true;
            }
        });
    },
    /**
     * Translate a client resource name to server resource name.
     *
     * There are 2 ways to define the server resource name (ordered by relevance):
     *
     * 1. On the relationship, eg Model.extend({relations : {
     *     author: {
     *         relationClass: MAuthor,
     *         serverResourceName: 'user',
     *     },
     * }})
     * 2. On the model using SERVER_RESOURCE_NAME, eg Model.extend({}, {SERVER_RESOURCE_NAME: 'some-server-name'})
     *
     * @param  {[type]} clientKey [description]
     * @return {[type]}           [description]
     */
    getServerResourceName(clientKey) {
        const relations = _.result(this, 'relations');

        if (typeof relations[clientKey] === 'object' && relations[clientKey].serverResourceName) {
            return relations[clientKey].serverResourceName;
        }

        return relations[clientKey].SERVER_RESOURCE_NAME ? relations[clientKey].SERVER_RESOURCE_NAME : clientKey;
    },
    /**
     * Make relations option uniform, eg turn everything in a object.
     *
     * @return {Array}
     */
    formatRelations(relations) {
        return relations.map((clientKeyOrObject) => {
            if (typeof clientKeyOrObject === 'string') {
                return {
                    relation: clientKeyOrObject,
                };
            }

            return clientKeyOrObject;
        });
    },
    /**
     * Save the model using multiput. The relations should be defined as
     * follows:
     *
     * relations = [
     *     // Simple key.
     *     'comments',
     *
     *     // Complex object.
     *     {
     *         relation: 'task',
     *         parseId(
     *             mTask,       // The object that should replace the negative id.
     *             newId,       // New id for object.
     *             idMapScoped  // Id map scoped to relation.
     *         ) {
     *             // Some callback action.
     *         }
     *     }
     * ];
     *
     * @param  {Array} Relations
     * @param  {Object} options
     */
    multiput(relationsUnformatted, options = {}) {
        const pResult = $.Deferred();
        const data = this.multiputBuildData(relationsUnformatted, options).data;

        // Save data.
        const xhr = $.ajax({
            url: options.url || this.urlRoot,
            data: options.multiputFormatData ? JSON.stringify(options.multiputFormatData(data)) : JSON.stringify(data),
            method: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            processData: false,
        });

        xhr.fail(pResult.reject);
        xhr.done((resp) => {
            if (resp.idmap) {
                this.idMap(relationsUnformatted, resp.idmap);
            }

            pResult.resolve(resp);
        });

        // Mimic backbone-crux/helper/sync.
        pResult.fail((jqXhr, textStatus, errorThrown) => this.trigger('after:save:error', jqXhr, textStatus, errorThrown));
        pResult.done((data, textStatus, jqXhr) => this.trigger('after:save:success', data, textStatus, jqXhr));
        pResult.always(() => this.trigger('after:save'));

        return pResult;
    },
    /**
     * TODO: why does this return id: id, data: actual data?
     */
    multiputBuildData(relationsUnformatted, options = {}, id = 0) {
        const appendToData = function(data, key, value) {
            if (data[key]) {
                data[key].push(value);
            } else {
                data[key] = [value];
            }
        };

        const multiputData = { data: [], with: {} };
        let relations = {};
        let def = {};

        if (Array.isArray(relationsUnformatted)) {
            relations = this.formatRelations(relationsUnformatted);
            def = {};
        } else {
            relations = this.formatRelations(relationsUnformatted.relations);
            def = relationsUnformatted;
        }


        const isEmptyId = function(maybeEmptyId) {
            return maybeEmptyId === null || maybeEmptyId === undefined;
        };

        // This model should also be id-mappable.
        if (isEmptyId(this.id)) {
            this.set(this.idAttribute, --id);
        }

        _.each(relations, (defRel) => {
            // Make negative ids for relations. Each negative id can later be
            // mapped to positive ids provided by the backend.
            if (this.get(defRel.relation) instanceof Backbone.Collection) {
                this.get(defRel.relation).each((mModel) => {
                    if (isEmptyId(mModel.get('id'))) {
                        mModel.set('id', --id);
                    }
                });
            } else if (this.get(defRel.relation) instanceof Backbone.Model) {
                if (isEmptyId(this.get(defRel.relation).get('id'))) {
                    this.get(defRel.relation).set('id', --id);
                }
            }

            if (this.get(defRel.relation) instanceof Backbone.Collection) {
                if (defRel.relations) {
                    this.get(defRel.relation).each(mModel => {
                        const d = mModel.multiputBuildData(defRel, options, id);
                        id = d.id;
                        appendToData(multiputData.with, this.getServerResourceName(defRel.relation), d.data.data[0]);

                        _.each(d.data.with, (valArray, relation) => {
                            _.each(valArray, (val) => {
                                appendToData(multiputData.with, relation, val);
                            });
                        });
                    });
                } else {
                    multiputData.with[this.getServerResourceName(defRel.relation)] = this.get(defRel.relation).toJSON(defRel.toJSONOptions);
                }
            } else {
                if (defRel.relations) {
                    const subMultiput = this.get(defRel.relation).multiputBuildData(defRel, options, id);
                    id = subMultiput.id;

                    multiputData.with[this.getServerResourceName(defRel.relation)] = [subMultiput.data.data[0]];
                    _.extend(multiputData.with, subMultiput.data.with);
                } else {
                    multiputData.with[this.getServerResourceName(defRel.relation)] = [this.get(defRel.relation).toJSON(defRel.toJSONOptions)];
                }
            }
        });

        // Support patch.
        if (options.patch && Array.isArray(options.patch)) {
            // Add idAttribute so server knows which model we are updating.
            multiputData.data = [_.pick(this.toJSON(def.toJSONOptions), _.union(options.patch, [this.idAttribute]))];
        } else {
            multiputData.data = [this.toJSON(def.toJSONOptions)];
        }

        return {
            id,
            data: multiputData,
        };
    },
});
