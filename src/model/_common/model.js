import MMultiput from '../multiput';
import { Collection } from 'backbone-crux';
import vent from 'vent';

export default MMultiput.extend({
    //+AAA
    getParams: null,
    initialize() {
        this.getParams = {};
    },
    /**
     * Handle redirect on urls that don't end with /.
     */
    url: function(options = {}) {
        _.defaults(options, { appendGetParams: true });
        let url = MMultiput.prototype.url.call(this);

        if (url.slice(-1) !== '/') {
            url += '/';
        }

        if (options.appendGetParams) {
            url = this.appendGetParams(url);
        }

        return url;
    },
    appendGetParams(url) {
        if (!_.isEmpty(this.getParams)) {
            return url + '?' + $.param(this.getParams);
        }

        return url;
    },
    //-AAA


    // TODO FIX FIX FIX!!!!
    setByModelOrCollection(relation, value, options) {
        if (relation instanceof Collection) {
            this.setByCollection(relation, value, options);
        } else {
            MMultiput.prototype.setByModelOrCollection.call(this, relation, value, options);
        }
    },

    attributesMeta: { id: 'int' },
    /**
     * Shorthand for sending a PATCH, essentially a this.save(attrs, {patch: true})
     * where attrs is build from attrKeys.
     *
     * @param  {Array} attrKeys Specify which keys of the model you which to send.
     * @param  {Object} options
     * @return {Promise}
     */
    patch(attrKeys, options = {}) {
        const attrs = this.toJSON();

        _.defaults(options, { patch: true });

        return this.save(_.pick(attrs, attrKeys), options);
    },
    parse(resp, options) {
        const HashMap = function(data) {
            this.get = function(id) {
                return this.byId[id];
            };

            this.add = function(someData) {
                _.each(someData, (item) => {
                    this.byId[item.id] = item;
                });
            };

            this.byId = {};
            this.add(data);
        };

        if (resp.with) {
            options.repository = new Backbone.Collection();

            _.each(resp.with, (data, key) => {
                const withMapping = [key];
                _.each(resp.with_mapping, (givenKey, requestedKey) => {
                    if (givenKey === key) {
                        withMapping.push(requestedKey.split('.').pop());
                    }
                });

                _.each(_.uniq(withMapping), (id) => {
                    if (options.repository.get(id)) {
                        options.repository.get(id).get('collection').add(data);
                    } else {
                        options.repository.add({
                            id,
                            collection: new HashMap(data),
                        });
                    }
                });
            });
        }

        // Quick & dirty hack for objects wrapped in data.
        if (resp.data) {
            _.extend(resp, resp.data);
        }

        return MMultiput.prototype.parse.call(this, resp, options);
    },
    restore() {
        return $.ajax({
            url: this.url(),
            method: 'POST',
            data: null,
        }).done(() => {
            this.set('deleted', false);
            vent.trigger('after:model:restore', this);
        });
    },
    destroy: function (options) {
        var xhr = MMultiput.prototype.destroy.call(this, options);

        xhr.done(() => this.set('deleted', true));

        return xhr;
    },
    /**
     * Extended clone where each relation will be cloned so that they
     * are indeed different objects.
     */
    clone: function () {
        var clone = MMultiput.prototype.clone.call(this);

        _.each(_.keys(_.result(this, 'relations')), function(relation) {
            clone.set(relation, clone.get(relation).clone(), { replace: true });
        });

        return clone;
    },
    /**
     * Create a response for the server based on _.result(this, 'relations'). If a
     * model is found, then take its id. If a collection is found, pluck
     * all ids. If a collection with join_attributes is found, pluck
     * all ids with join attributes.
     *
     * @return {[type]} [description]
     */
    toJSON: function () {
        const attrs = MMultiput.prototype.toJSON.apply(this, arguments);

        _.each(_.result(this, 'relations'), (config, relation) => {
            if (this.get(relation) instanceof MMultiput) {
                attrs[relation] = this.get(relation).get('id');
            } else if (this.get(relation) instanceof Collection) {
                if (config.join_attributes) {
                    _.each(attrs[relation], function(modelJson, index) {
                        attrs[relation][index] = _.pick(modelJson, _.union(['id'], config.join_attributes));
                    });
                } else {
                    attrs[relation] = this.get(relation).pluck('id');
                }
            }
        });

        return _.omit(attrs, ['meta', 'data', 'debug', 'with', 'with_mapping', 'with_related_name_mapping', '_id']);
    },
    toHuman: function () {
        var attrs = MMultiput.prototype.toHuman.call(this);

        return _.omit(attrs, '_meta', 'debug');
    },
    /**
     * @param  {[type]} attributes [description]
     * @param  {[type]} options    [description]
     * @return {Array} Array of changed attributes.
     */
    formatAttributes(attributes, options) {
        const attrs = MMultiput.prototype.formatAttributes.call(this, attributes, options);

        return this.formatAttributesId(attrs, options);
    },
    formatAttributesId(attributes) {
        if (attributes !== undefined && attributes !== null) {
            if (attributes.id && this.attributesMeta.id === 'int') {
                attributes.id = parseInt(attributes.id, 10);

                // Set to null if parse didn't work or else isNew fails.
                if (isNaN(attributes.id)) {
                    attributes.id = null;
                }
            } else if (attributes.id === '') {
                attributes.id = null;
            }
        }

        return attributes;
    },
    /**
     * Extract ids from item.
     *
     * {dpsk0231}
     *
     * @param {String|Integer|Object|Model|Collection} item
     * @return {Array} Array of ids.
     */
    extractIds(item) {
        let ids = [];

        if (Array.isArray(item)) {
            _.each(item, function(maybeId) {
                if (_.isObject(maybeId)) {
                    ids.push(maybeId.id);
                } else {
                    ids.push(maybeId);
                }
            });
        } else if (item instanceof MMultiput) {
            ids.push(item.get('id'));
        } else if (item instanceof Collection) {
            ids = item.pluck('id');
        } else {
            ids.push(item);
        }

        return ids;
    },
    // +++ helpers ++++
    /**
     * Parses strings to integers. Returns defaultVal if val is NaN or
     * infinity.
     *
     * @param {string} val
     * @param {mixed} defaultVal Defaults to null.
     * @return {integer}
     */
    fromHumanInt(val, defaultVal) {
        const result = parseInt(val, 10);

        if (defaultVal === undefined) {
            defaultVal = null;
        }

        return !_.isNumber(result) || isNaN(result) ? defaultVal : result;
    },
    fromHumanIntOrNull(val) {
        return this.fromHumanInt(val, null);
    },
    fromHumanBoolOrNull(val) {
        switch (val) {
            case '':
                return null;
            case true:
            case 'yes':
            case 'true':
                return true;
            case false:
            case 'no':
            case 'false':
                return false;
            default:
                return false;
        }
    },
    fromHumanBool(val, defaultValue) {
        const bool = this.fromHumanBoolOrNull(val);

        return bool === null ? defaultValue : bool;
    },
    fromHumanChoice(val, choices, defaultValue) {
        if (choices.indexOf(val) !== -1) {
            return val;
        }

        return defaultValue;
    },
    fromHumanChoiceOrNull(val, choices) {
        return this.fromHumanChoice(val, choices, null);
    },
    /**
     * Checks whether a model or integer (like) is given and returns
     * the id of that model.
     *
     * @param {mixed} val The value to check against.
     * @param {mixed} model Value can be instance of this model.
     * @return {int|null}
     */
    fromHumanModelIdOrNull(val, model) {
        if (val instanceof model) {
            return val.get('id');
        }

        return this.fromHumanIntOrNull(val);
    },
    copy() {
        const attrs = _.clone(this.attributes);

        _.each(_.result(this, 'relations'), (props, key) => {
            if (this.get(key).copy) {
                attrs[key] = this.get(key).copy();
            }
        });

        return new this.constructor(attrs);
    },
     /**
     * Shorthand for getting nested attributes.
     *
     * @param {string} key Attribute name in dot notation.
     * @return {mixed} The value of key if found, undefined otherwise.
     */
    comma(key) {
        if (typeof key !== 'string') {
            return undefined;
        }

        const keys = key.trim('.').split('.');
        let result = this;

        _.some(keys, (anotherKey) => {
            if (result && typeof result.get === 'function') {
                result = result.get(anotherKey);
                return false;
            } else if (result !== null && typeof result === 'object') {
                result = result[anotherKey];
                return false;
            }

            result = undefined;
            return true;
        });

        return result;
    },
    // --- helpers ----
});
