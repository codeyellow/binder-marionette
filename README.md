# Changelog

## 0.2.3
- Add `isStateChanged`.
- Update url before sending request.
- Fix paging not being persisted issue.

## 0.1.31
- Fix 0.1.28 commit issue.

## 0.1.30
- Improve SelectionItem rendering.
 
## 0.1.29
- Initialize phone when rendering `view/phone`.

## 0.1.28
- Add `bindingOptions` to SelectStickit.
 
## 0.1.27
- Add selectionItem and selectionTracker.

## 0.1.26
- Improved SelectStickit bindings.

## 0.1.25
- Add `first_name` and `last_name` for `view/user/edit.js`.

## 0.1.24
- Fix exports hack for webpack 2.

## 0.1.23
- Fixed absolute path in `view/user/edit.js`.

## 0.1.22
- Add change / forgot password + profile pages.

## 0.1.21
- Add `view/user/edit.js`.
 
## 0.1.20
- Respect `options.url` when multiputting.

## 0.1.19
- Add duration to `view/phone`.

## 0.1.18
- Revert accidental overwrite.

## 0.1.17
- Add `view/phone`.

## 0.1.16
- Extract own id from idMap without needing multiputIdServerKey.

## 0.1.15
- Unify fetching bootstrap.

## 0.1.14
- Expose fetching bootstrap.
 
## 0.1.13
- Add same arguments to `after:save:success` / `after:save:error` when using multiput.

## 0.1.11
- Added helper functions for dealing with myView.

## 0.1.10
- Added `view/myView`.

## 0.1.9
- Order is now always defined because order_by `-id` is injected when needed.

## 0.1.8
- Fixed typo. 

## 0.1.7
- Add `view` argument to `createSourceCollection`. 

## 0.1.6
- Add `view` argument to `getTargetModel`. 

## 0.1.5
- Fixed url encode / decode madness.
- Use `change` instead of `before:read` for Backbone.Collection.State.
- Fixed support for `this.relation` being a function.

## 0.1.4
- Add selectStickit.`targetModel`.

## 0.1.3
- Add support for `this.relation` being a function.

## 0.1.0
- Changed SelectStickit API.

## 0.0.12
- Added `isChanged` to behavior/applyButton.

## 0.0.11
- Changed env bar to avoid CSP inline style errors.

## 0.0.10
- Add selectStickit stickitSelectOptions.
 
## 0.0.9
- Add selectizeList behavior.
 
## 0.0.8
- Add selectStickit behavior.
